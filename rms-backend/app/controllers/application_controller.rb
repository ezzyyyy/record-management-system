class ApplicationController < ActionController::API
    include JWTSessions::RailsAuthorization
    rescue_from JWTSessions::Errors::Unauthorized, with: :not_authorized

    private
        # Payload comes from JWTSessions
        # Not using device and creating our own (?)
        def current_user
            @current_user ||= User.find(payload['user_id'])
        end

        # This method is created for when non-authorized users try to access data
        def not_authorized
            render json: { error: 'Not authorized' }, status: :unauthorized
        end
end
