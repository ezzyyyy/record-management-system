import axios from 'axios'

const API_URL = 'http://localhost:3000'

const securedAxiosInstance = axios.create({
    baseURL: API_URL,
    withCredentials: true,
    headers: {
        'Content-Type': 'application/json',
    }
})

const plainAxiosInstance = axios.create({
    baseURL: API_URL,
    withCredentials: true,
    headers: {
        'Content-Type': 'application/json'
    }
})

securedAxiosInstance.interceptors.request.use(config => {
    const method = config.method.toUpperCase();
    if (method !== 'OPTIONS' && method !== 'GET') {
        config.headers = {
            ...config.headers,
            'X-CSRF-TOKEN': localStorage.csrf
        }
    }

    return config
})

securedAxiosInstance.interceptors.response.use(null, error => {
    // If cookie is expired or if error status is 401, return refresh request
    if (error.response && error.response.config && error.response.status === 401) {
        // Post refresh controller on API
        return plainAxiosInstance.post(
            '/refresh',
            {},
            { headers: { 'X-CSRF-TOKEN': localStorage.csrf } })
            .then(response => {
                localStorage.csrf = response.data.csrf // Save token locally
                localStorage.signedIn = true

                let retryConfig = error.reponse.config
                retryConfig.headers['X-CSRF-TOKEN'] = localStorage.csrf
                return plainAxiosInstance.request(retryConfig)
            }).catch(error => {
                delete localStorage.csrf // Delete session
                delete localStorage.signedIn

                location.replace('/')
                return Promise.reject(error)
            })
    } else {
        return Promise.reject(error)
    }
})

export { securedAxiosInstance, plainAxiosInstance }