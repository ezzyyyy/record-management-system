# Be sure to restart your server when you modify this file.

# Avoid CORS issues when API is called from the frontend app.
# Handle Cross-Origin Resource Sharing (CORS) in order to accept cross-origin AJAX requests.

# Read more: https://github.com/cyu/rack-cors

Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do

    # For communication with VueJS Front-end
    origins 'http://localhost:8080'

    resource '*',
      headers: :any,
      credentials: true, # Important for request Sign-in, Sign-up responses 
      methods: [:get, :post, :put, :patch, :delete, :options, :head]
  end
end
