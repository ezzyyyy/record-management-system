class SigninController < ApplicationController
  # Authenticate user
  # Validates access token within the request
  # Important when you want the user to be signed in to do something
  before_action :authorize_access_request!, only: [:destroy] 

  def create
    user = User.find_by!(email: params[:email]) # Find by email, return if found

    if user.authenticate(params[:password])
      payload = { user_id: user.id }

      # "refresh_by_access_allowed" - If there are issues, refresh the session and ask user to sign in again
      session = JWTSessions::Session.new(payload: payload, refresh_by_access_allowed: true) 
      tokens = session.login

      # When using cookies, vulnerability to CSRF
      # That is why both the login and refresh methods of the Session class produce CSRF tokens for you
      response.set_cookie(JWTSessions.access_cookie,
                          value: tokens[:access],
                          httponly: true,
                          secure: Rails.env.production?)

      render json: { csrf: tokens[:csrf] }
    else
      not_found
    end
  end

  def destroy 
    session = JWTSessions::Session.new(payload: payload)
    session.flush_by_access_payload
    render json: :ok
  end

  private 

    def not_found
      json: { error: "Cannot find email/password combination" }, status: :not_found
    end
end