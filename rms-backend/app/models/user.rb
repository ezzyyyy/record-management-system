class User < ApplicationRecord
    has_secure_password # From bcrypt
    has_many :records
end
